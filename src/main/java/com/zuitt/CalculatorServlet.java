package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException {
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been initialized. ");
		System.out.println("******************************************");
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1> <br> <br> To use the app, input two numbers and an operation. <br><br> Hit the submit button after filling in the details. <br> <br> You will get the result shown in your browser! ");
		}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
        
        
        int num1= Integer.parseInt(req.getParameter("num1"));
        int num2= Integer.parseInt(req.getParameter("num2"));
        String operator = req.getParameter("operator");
        int result = 0;
        switch(operator) {
        	case "add":
        		result = num1 + num2;	
        	break;
        	
        	case "subtract":
        		result = num1 - num2;	
        	break;
        	
        	case "multiply":
        		result = num1 * num2;	
        	break;
        	
        	case "divide":
        		result = num1 / num2;	
        	break;
        	
        	default:
        		System.out.println("Operator does not exist");
        }
      
        PrintWriter out = res.getWriter();
        
        out.println("The two numbers you provided are: " + num1+ ", " +num2+ "\n\n The operation you wanted is: " +operator+ "\n\n The result is: " +result);
    }
	
	public void destroy() {
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been destroyed. ");
		System.out.println("******************************************");
	}

}
